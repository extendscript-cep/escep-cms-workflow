# ESCEP CMS Workflow

At its core, ESCEP CMS Workflow's idea is to connect publishing workflows across media borders seamlessly. Presently, its focus is on print and web channels. However, we are open to expanding the area.

## Any source, one workflow, standardized

Today's "publishing system" can be a wide range of applications – from CRM to publishing systems like Woodwing or vjoon K4. The ESCEP XML workflow provides a way to connect them in one workflow with popular CMS like Drupal or WordPress in a standardized manner that is simple but not bound to semantic content structures like DITA. To achieve this, we use IPTC's NITF standard for XML files and implement it in a manner that can be used to translate the internal information architecture of an Adobe InDesign document or a CRM entry into its equivalent HTML 5 semantics. As a result, you don't need to implement a CMS module or plugin for each content provider. Instead, you can connect the content of any source through one Drupal Module or WordPress Plugin.

***To make things as simple as possible, we provide the required Drupal Module and WordPress Plugin as open-source projects for free.*** Don't hesitate to give it a try. You can implement the entire workflow if you are familiar with XML transformation and publishing workflows. ***If you need support or want to outsource your project, then our project partners are ready got get in touch.*** Please contact us as mentioned in the [project description's "product and services."](https://gitlab.com/extendscript-cep/project#projects-services)

![escep cms workflow](./assets/workflow/escep-cms-workflow.png)

## The XML

We use [IPTC](https://iptc.org/)'s [NITF](https://iptc.org/standards/nitf/) schema to structure the content for the import.

### Documentation and Demo

You can find a ***detailed annotated example of the required appliance*** of the XML structure in [`./xml/docs/index.xml`](./xml/docs/index.xml). If you are looking for a complete demo of the required XML and folder structure, please download [`./xml/example/demo.zip`](./xml/example/demo.zip).

### Rules

#### Folder name

The archive of the content folder must have its contents in the root or a sub-directory sharing the exact same name. Nesting content folders are not supported! The folder name does not matter as long as it is POSIX compatible.

```bash
# Works ✅

example-1234.zip

# ... extraction ...

.
├── yourfile.xml
└── images/
```

<hr>

```bash
# Works ✅

example-1234.zip

# ... extraction ...

example-1234
├── yourfile.xml
└── images/
```

<hr>

```bash
# Does NOT work ❌

example-1234.zip

# ... extraction ...

hello-world ❌
├── subfolder/ ❌
│   ├── yoursubfile.xml ❌
│   └── images/ ❌
├── yourfile.xml
└── images/
```

#### One piece of content per folder

We do not support multiple XML files per piece of content. Every archive of content must have exactly one XML file and zero, one or multiple folders for media and assets.

```bash
# Works ✅
.
├── yourfile.xml
└── images/
```

<hr>

```bash
# Works ✅
.
├── yourfile.xml
├── assets/
└── images/
```

<hr>

```bash
# Does NOT work ❌
.
├── yourfile.xml
├── anotherfile.xml ❌
└── images/
```

#### One Article per XML file

***We do not support multiple articles per XML file.*** Each XML file can contain exactly one piece of content, like a post or a page.

#### Link media and assets with relative paths

We expect relative links to media objects. Absolute paths are not supported and will be ignored. Suppose, the XML looks like that:

```xml
<media media-type="image">
  <media-reference source="./images/36232_36230.jpg"/>
  <media-caption>A short description</media-caption>
</media>
```

In this case, we expect (and it is best practice) to have this folder structure:

```bash
.
├── yourfile.xml
└── images
    └── 36232_36230.jpg
```

The name of your XML file does not matter. Instead, you must pack every piece of content in a correctly bundled archive that creates the correct folder structure after extraction.

#### Wrap the content into block elements

Even if it is valid to place media elements directly in the body content element, we do not support that. Images or contents placed outside of block elements will be ignored.

```xml
<!-- Works ✅ -->
<nitf>
  <body>
    <body.content>
      <block>
        <p style="p">This is a text.</p>
        <media media-type="image">
          <media-reference source="./images/36232_36229.jpg"/>
          <media-caption>A short description</media-caption>
        </media>
      </block>
      <block>
        <media media-type="image">
          <media-reference source="./images/36232_36230.jpg"/>
          <media-caption>A short description</media-caption>
        </media>
      </block>
    </body.content>
  </body>
</nitf>
```

<hr>

```xml
<!-- Does NOT work ❌ -->
<nitf>
  <body>
    <body.content>
      <block>
        <p style="p">This is a text.</p>
      </block>
   ❌ <media media-type="image">
   ❌   <media-reference source="./images/36232_36230.jpg"/>
   ❌   <media-caption>A short description</media-caption>
   ❌ </media>
    </body.content>
  </body>
</nitf>
```

## Questions & Support

Do you have a question or found a bug? For technical questions or bugs, please [open an Issue](https://gitlab.com/extendscript-cep/escep-cms-workflow/-/issues) (a free GitLab Account is required) or send an e-mail to `hello@ansilabs.ch`. For paid support or consulting requests, don't hesitate to get in touch with `info@pantara.ch`.

Please note: this is an open-source project under the GNU GPLv3 license. We don't give any warranty nor give any reliable deadlines. If you want your feature request prioritized, you can sponsor its development to get it done fast and with a reliable deadline. Please get in touch with `info@pantara.ch`.

## License

ESCEP CMS Workflow  
Copyright (C) 2022–present Andreas (development@meen.ch), and Contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

